/*$(document).ready(function() {

    var addFunction = function() {
        var title = $(this).parent().text().trim();
        console.log(title);
        $.ajax({
            type: 'POST',
            url: '/article/addbookmark/',
            data: {
                'title': title
            },
            success: function(msg) {
                Materialize.toast(msg, 1500, 'rounded');
            },
            failure: function() {}
        });
    };

    $(".button-collapse").sideNav();
});*/

// Scroll globals
var pageNum = 2; // The latest page loaded | THIS IS 2 BECAUSE FIRST TIME 2O ARE RENDERED
var hasNextPage = true; // Indicates whether to expect another page after this one

function loadMoreArticles() {
    // If the next page doesn't exist, just quit now 
    if (hasNextPage === false) {
        return false
    }
    // Update the page number
    pageNum = pageNum + 1;
    // Configure the url we're about to hit
    $.ajax({
        url: '/account/load_articles/',
        data: {page_number: pageNum},
        dataType: 'json',
        success: function(data, status, xhr) {
            var ct = xhr.getResponseHeader("content-type") || "";
            if (ct == 'application/json') {
                var articles = JSON.parse(data);
                if(articles.length < 1) {

                } else {
                    var list = document.getElementById('articles');
                    for (var i in articles) {
                        title = articles[i].fields.title;
                        link = articles[i].fields.destination_url
                        description = articles[i].fields.description;
                        score = articles[i].fields.vocab_score;
                        wordsobj = JSON.parse(articles[i].fields.contained_words);


                        var card = document.createElement('div');
                        card.setAttribute('class', 'col s12 m12 l12 card-panel hoverable');

                        var card_content = document.createElement('div');
                        card_content.setAttribute('class', 'card-content ');

                        var card_title_div = document.createElement('div')
                        card_title_div.setAttribute('class', 'row')

                        var card_title = document.createElement('a')
                        card_title.setAttribute('class', 'card-title col s11 m11 l11 center-align article_title blue-text text-darken-2');
                        card_title.setAttribute('href', link);
                        card_title.setAttribute('target', '_blank');
                        card_title.textContent = title;
                        card_title_div.appendChild(card_title);


                        var card_bookmark = document.createElement('i');
                        card_bookmark.setAttribute('class', 'col s1 m1 l1 fa fa-bookmark tooltipped bookmark_icon');
                        card_bookmark.setAttribute('data-position', 'bottom');
                        card_bookmark.setAttribute('data-tooltip', 'Bookmark this article');
                        card_bookmark.addEventListener('click', bookmarkArticle, false);
                        card_title_div.appendChild(card_bookmark);

                        card.appendChild(card_title_div);

                        var card_description = document.createElement('p');
                        card_description.textContent = description;
                        card_content.appendChild(card_description);

                        var wordlist = document.createElement('ul');
                        wordlist.style.display = 'flex';
                        wordlist.innerHTML = "Good Words:";
                        wordlist.appendChild( document.createTextNode( '\u00A0 \u00A0' ) );
                        wordlist.appendChild
                        for (var i in wordsobj){
                            var word = document.createElement('li');
                            word.textContent = wordsobj[i] + ', ';
                            wordlist.appendChild(word);
                        }

                        card_content.appendChild(wordlist);

                        card.appendChild(card_content);

                        var card_footer = document.createElement('div');
                        card_footer.setAttribute('class', 'card-action');

                        var card_score = document.createElement('span');
                        card_score.textContent = "Vocab Score: " + score;
                        card_score.setAttribute('class', 'right');

                        card_footer.appendChild(card_score);

                        card.appendChild(card_footer);


                        list.appendChild(card);
                        
                    }
                    $('.tooltipped').tooltip({delay: 50});
                }
            }
            // Update global next page variable
            hasNextPage = true;//.hasNext;
            // Loop through all items
            //for (i in data) {
              //  console.log(i);
                // Do something with your json object response
            //}
        },
        error: function(data) {
            // When I get a 400 back, fail safely
            hasNextPage = false
        },
        complete: function(data, textStatus){
            // Turn the scroll monitor back on
            console.log(pageNum);
        }
    });
}

/*$(document).on('submit','filter',function(e){
    e.preventDefault();
    var filter_form = $('#filter');
    $.ajax({
            type: 'POST',
            url: '/article/filterarticles/',
            data: filter_form.serialize(),
            success: function(context, status, xhr) {
                console.log('In ajax');
                var ct = xhr.getResponseHeader("content-type") || "";
                if (ct == 'application/json') {
                    var articles = JSON.parse(context);
                    for (var article in articles) {
                        console.log(article);
                    }
                }
                console.log("SUCCESS");
            },
            failure: function() {
                console.log('Panga ho gya!');
            }
        });
});*/