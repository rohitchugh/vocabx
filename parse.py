from bs4 import BeautifulSoup
import xml.etree.cElementTree as ET
from http.cookiejar import CookieJar
import urllib.request as req
import requests
import re
import time
import sqlite3
import json
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "GoodWord.settings")
from article.models import Article
check_list = Article.objects.values_list('title')
check_dict = dict((title[0], 1) for title in check_list)

cj = CookieJar()
opener = req.build_opener(req.HTTPCookieProcessor(cj))
opener.addheaders = [('User-agent', 'Mozilla/5.0')]

conn = sqlite3.connect('words.sqlite')
cur = conn.cursor()

#GET XML DATA
urls = 'article/rss.txt'
artdict = dict()
def getXML(f):
	f_xml = open(f, 'r')
	for line in f_xml:
            try:
                page = requests.get(line.strip())
            except:
                print("Error in url: ", line.rstrip())
                continue
            try:
                tree = ET.XML(page.content)
            except:
                print("ERROR:", line)
                continue
            e = tree.findall('channel')
            x = e[0].findall('item')
            raw_source = line.split('.')
            source = raw_source[1]

            if (source == 'newyorker'):
                    dummy = raw_source[2].split('/')
                    raw_genre = dummy[len(dummy)-1]
            elif (source == 'nytimes'):
                    dummy = raw_source[2].split('/')
                    raw_genre = dummy[len(dummy)-1]
            elif (source == 'theguardian'):
                    dummy = raw_source[2].split('/')
                    raw_genre = dummy[2]
            elif (source == 'thehindu'):
                    dummy = raw_source[2].split('/')
                    raw_genre = dummy[1]
            else:
                    raw_genre = 'others'

            raw_genre = raw_genre.strip()
            if (raw_genre == 'news'):
                    genre = 'News'
            elif(raw_genre == 'tech'or raw_genre == 'Technology'or raw_genre == 'technology' or raw_genre == 'sci-tech'):
                    genre = 'Technology'
            elif(raw_genre == 'sporting-scene' or raw_genre == 'Sports' or raw_genre == 'sport'):
                    genre = 'Sports'
            elif(raw_genre == 'book' or raw_genre == 'Books' or raw_genre == 'arts'):
                    genre = 'Books'
            elif(raw_genre == 'culture' or raw_genre == 'Culture'):
                    genre = 'Culture'
            elif(raw_genre == 'business' or raw_genre == 'Business'):
                    genre = 'Business'
            elif(raw_genre == 'health' or raw_genre == 'Health'):
                    genre = 'Health'
            elif(raw_genre == 'world' or raw_genre == 'World'):
                    genre = 'World'
            elif(raw_genre == 'education' or raw_genre == 'Education'):
                    genre = 'Education'
            elif(raw_genre == 'science' or raw_genre == 'Science'):
                    genre = 'Science'
            elif(raw_genre == 'Politics'):
                    genre = 'Politics'
            elif(raw_genre == 'opinion'):
                    genre = 'Opinion'
            elif(raw_genre == 'magazine'):
                    genre = 'Magazine'
            elif(raw_genre == 'cartoons'):
                    genre = 'Cartoons'
            elif(raw_genre == 'humor'):
                    genre = 'Humor'
            elif(raw_genre == 'MostViewed' or raw_genre == 'US' or raw_genre == 'others'):
                    genre = 'Others'
            else:
                    genre = 'Others'

            for item in x:
                for foo in item:
                    if(foo.tag == 'title'):
                        if (foo.text == None or foo.text in check_dict):
                            print(foo.text)
                            if foo.text == ' ':
                                print("SHIT")
                                exit()
                            continue
                        else:
                            artdict[foo.text] = {}

            title = ''
            for item in x:
                for foo in item:
                    if(foo.tag == 'title'):
                        if (foo.text == None or foo.text in check_dict):
                            print("Title: ", title)
                            continue
                        title = foo.text
                    else:
                        try:
                            tag = foo.tag
                            artdict[title][tag] = foo.text
                        except:
                            continue
                            print("TITLE: ", title, "TAG: ", foo.tag, "TAG TEXT: ", foo.text)
                            print("EXIT2")
                            exit()
                artdict[title]['word_list'] = list()
                artdict[title]['vocab_score'] = 0
                artdict[title]['source'] = source
                artdict[title]['genre'] = genre
                if (artdict[title]['description'] != None):
                    artdict[title]['description'] = artdict[title]['description'].lstrip('<p>').rstrip('</p>')
            print("Completed: ", line)
	f_xml.close()
	return


# READ XML DATA
def readXML():
    for item in artdict:
        print("TITLE: ", item)
        print("DESCRIPTION: ", artdict[item]['description'])
        print("LINK: ", artdict[item]['link'])
        try:
            date = artdict[item]['pubDate'].split(' ')
            pubdate = date[1] + ' ' + date[2] + ' ' + date[3]
            print("PUBLICATION DATE: ", pubdate)
        except:
            print("!!!!!!")
            print("PUBLICATION DATE: ", artdict[item]['pubDate'])
        print("SOURCE: ", artdict[item]['source'])
        print("GENRE: ", artdict[item]['genre'])
        print('\n')

# BUILD DICTIONARY
mydict = dict()
def buildDict():
    print("Build Dict")
    q = cur.execute('SELECT Wordlist.word, Definitions.def FROM Wordlist INNER JOIN Definitions ON Wordlist.id = Definitions.wordlist_id')
    for item in q.fetchall():
        mydict[item[0]] = item[1]
    print(len(mydict))

# READ ARTICLES
def readArticles():
    words = []
    artwords = []
    print("In read articles")
    for item in artdict:
        try:
            link = artdict[item]['link']
            url = req.Request(link, data = None, headers = {
        'User-Agent': 'Mozilla/5.0 Chrome/35.0.1916.47 Safari/537.36'
    })
        except SocketError as e:
            if e.errno != errno.ECONNRESET:
                raise
            pass
        wordcount = 0
        try:
            page = opener.open(url)
        except:
            continue
        soup = BeautifulSoup(page, 'lxml')
        links = soup.find_all('p')
        for tag in links:
            words.append(re.findall(r"[\w]+", tag.getText()))
        for line in words:
            for checkword in line:
                if checkword in mydict:
                    artwords.append(checkword)
        artwords = list(set(artwords))
        wordcount = len(artwords)
        print(artwords)
        for addword in artwords:
            artdict[item]['word_list'].append(addword)
        print(wordcount)
        artdict[item]['vocab_score'] = wordcount
        del words[: ]
        del artwords[: ]
    return

# READ ARTICLE DICTIONARY
def readDict():
    for item in artdict:
        print("TITLE: ", item)
        print("DESCRIPTION: ", artdict[item]['description'])
        print("LINK: ", artdict[item]['link'])
        try:
            date = artdict[item]['pubDate'].split(' ')
            pubdate = date[1] + ' ' + date[2] + ' ' + date[3]
            print("PUBLICATION DATE: ", pubdate)
        except:
            print("!!!!!!")
            print("PUBLICATION DATE: ", artdict[item]['pubDate'])
        print("VOCAB SCORE: ", artdict[item]['vocab_score'])
        print("WORDS CONTAINED: ", artdict[item]['word_list'])
        print("SOURCE: ", artdict[item]['source'])
        print("GENRE: ", artdict[item]['genre'])
        print('\n')

# CONVERT DATA TO JSON
def convertToJson():
    path = '/home/rohit/Dropbox/GoodWord'
    filename = 'articleData.json'
    filepath = os.path.join(path, filename)
    with open(filepath, "w") as writeJSON:
        json.dump(artdict, writeJSON)

# CALL FUNCTIONS
getXML(urls)
#readXML()
buildDict()
readArticles()
readDict()
convertToJson()
