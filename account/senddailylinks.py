from .models import MyUser
from article.models import Article
from django.core.mail import send_mass_mail, EmailMultiAlternatives, get_connection
from django.template.loader import render_to_string
from django.utils.html import strip_tags
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "GoodWord.settings")

def senddailylinks():
    connection = get_connection()
    connection.open()
    userlist = MyUser.objects.filter(is_active=1)
    bcclist = []
    for user in userlist:
        bcclist.append(user.email)
    articlelist = Article.objects.all()[0:10]
    message = ''' These are your daily article links \n''' 
    html_content = render_to_string('dailylinks.html', {'articlelist':articlelist})
    text_content = strip_tags(html_content)
    msg = EmailMultiAlternatives('Your Daily Articles', text_content, 'dailygoodwords@gmail.com', None, bcc=bcclist, connection=connection)
    msg.attach_alternative(html_content, "text/html")
    msg.send()
    connection.close()

senddailylinks()
