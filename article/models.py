from __future__ import unicode_literals
from django.db import models
from datetime import datetime
from django.utils.timezone import now

# Create your models here.

class Article(models.Model):
    title = models.CharField(max_length=200)
    source_name = models.CharField(max_length=20, null=True)
    description = models.TextField(null=True)
    publication_date = models.DateField(default=now)
    destination_url = models.TextField()
    vocab_score = models.IntegerField(null=True)
    rank = models.FloatField(null=False, default=0.0)
    contained_words = models.TextField()
    genre = models.CharField(max_length=20, default='Others')
    date_added = models.DateTimeField(default=now)

    def __str__(self):
        return self.description

    
class CachedFailures(models.Model):
    destination_url = models.TextField()
