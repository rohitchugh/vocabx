$('#url_submit_btn').click(function () {
   var own_url = $('#own_url')[0]['value'];
    var csrf = $('#ownurlform').serializeArray()[0]['value'];
    $.ajax({
        type: 'POST',
        url: '/article/addOwnArticle/',
        data: {
            csrfmiddlewaretoken: csrf,
            'own_url': own_url
        },
        success: function (msg) {
            Materialize.toast(msg, 1500, 'rounded');
        },
        failure: function () {
            Materialize.toast('Unable to process given link. Try again later.');
        }
    });
});