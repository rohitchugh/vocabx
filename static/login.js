$(document).ready(function(){
     $('.modal').modal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: 0.1,
      inDuration: 300, // Transition in duration
      outDuration: 200, // Transition out duration
      startingTop: '4%', // Starting top style attribute
      endingTop: '10%', // Ending top style attribute
      ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
      },
    });
    var width_new = $('.navbar-fixed').width();
    $('nav').width = width_new;
});


$('.js-validate-username').change(function () {
	var username = $(this);
	var form = $(this).closest("form");
      $.ajax({
        type: 'POST',
        url: form.attr("data-validate-username-url"),
        data: form.serialize(),
        dataType: 'json',
        success: function (data) {
          if (data.is_taken) {
            alert(data.error_message);
            username[0].style.color = "red";
          }
          else{
          	username[0].style.color = "white";
          }
      }
      });
});

function moveDown () {
    console.log('hello');
}