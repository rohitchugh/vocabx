from django import forms
from material import *

class filterForm(forms.Form):
    GENRE = (('News', 'News'),
            ('Technology','Technology'),
            ('Sports', 'Sports'),
            ('Education', 'Education'),
            ('Health', 'Health'),
            ('Business', 'Business'),
            ('World', 'World'),
            ('Politics', 'Politics'),
            ('Science', 'Science'),
            ('Culture', 'Culture'),
            ('Opinion', 'Opinion'),
            ('Magazine', 'Magazine'),
            ('Cartoon', 'Cartoon'),
            ('Humour', 'Humour'),
            ('Others', 'Others'),
            )
    genre = forms.MultipleChoiceField(choices=GENRE, widget=forms.CheckboxSelectMultiple())
