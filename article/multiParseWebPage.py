'''
    Python3
'''
from bs4 import BeautifulSoup
from http.cookiejar import CookieJar
from multiprocessing import Pool
from multiParseXML import xml
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import urllib.request as req
import sqlite3
import json
import os

cj = CookieJar()
opener = req.build_opener(req.HTTPCookieProcessor(cj))
opener.addheaders = [('User-agent', 'Mozilla/5.0')]

def buildDict():
    '''
        Build Dictionary of Words
    '''
    print("Build Dict")
    conn = sqlite3.connect('words.sqlite')
    cur = conn.cursor()
    q = cur.execute('SELECT Wordlist.word, Definitions.def FROM Wordlist INNER JOIN Definitions ON Wordlist.id = Definitions.wordlist_id')
    global mydict_list
    mydict_list = [item[0] for item in q.fetchall()]

def readArticle(title):
    '''
        Read Collected Articles
    '''
    global master_xml
    global stop_words
    global mydict_list
    try:
        link = master_xml[title]['link']
        url = req.Request(link, data = None, headers = {
        'User-Agent': 'Mozilla/5.0 Chrome/35.0.1916.47 Safari/537.36'
    })
        page = opener.open(url).read()
    except:
        return False
    
    soup = BeautifulSoup(page, 'lxml')
    soup = soup.find_all("p")
    text = ' '.join([i.string if i.string is not None else '' for i in soup])

    raw_tokens = word_tokenize(text)
    tokens = [w.lower() for w in raw_tokens if w.lower() not in stop_words and w.isalpha()]
    good_words = [token for token in set(tokens) if token in mydict_list]
    
    master_xml[title]['word_list'] = good_words
    master_xml[title]['vocab_score'] = len(good_words)
    print(title, wordcount, '\n')
    return True

def reading():
    '''
        Multi Processing Articles
    '''
    global master_xml
    how_many = 10
    p = Pool(processes=how_many)
    parse_us = list(master_xml.keys())
    print("Number of articles: ", len(parse_us))
    data = p.map(readArticle, [key for key in parse_us])
    p.close()

def convertToJson():
    '''
        Save Processed Article into JSON File
    '''
    global master_xml
    path = '/home/rohit/Dropbox/GoodWord'
    filename = 'articleData.json'
    filepath = os.path.join(path, filename)
    with open(filepath, "w") as writeJSON:
        json.dump(master_xml, writeJSON)

if __name__ == '__main__':
    '''
     Master XML
    '''
    master_xml = xml()

    '''
        Stop Words (English)
    '''
    stop_words = set(stopwords.words("english"))

    '''
        My Sample Dictionary
    '''
    mydict_list = list()

    buildDict()
    reading()
    convertToJson()
