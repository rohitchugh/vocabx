from datetime import date, timedelta
from math import log
import os
import django
import operator
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "GoodWord.settings")
django.setup()

from article.models import Article
check_list = Article.objects.all()

epoch = date(1970, 1, 1)
x = dict()
sorted_x = list()

def epoch_seconds(date):
    td = date - epoch
    return td.days * 86400

def hot(score, date):
    order = log(max(abs(score), 1), 10)
    sign = 3 if score > 15 else 2 if score >10 else 1 if score >5 else-1 if score <= 0 else 0
    seconds = epoch_seconds(date) - 1134028003
    return round(sign * order + seconds / 45000, 7)

for i in check_list:
    score = hot(i.vocab_score, i.publication_date)
    x[i.title] = score
    print(i.rank)
    i.rank = score
#    print(i.rank)
#    i.save()
#    print('saved')
sorted_x = sorted(x.iteritems(), key=operator.itemgetter(1))

for i in sorted_x[::-1]:
    print i[1], i[0].encode('utf-8')
