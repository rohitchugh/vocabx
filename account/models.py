from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import AbstractUser
from article.models import Article
from functools import partial
# Create your models here.

class MyUser(AbstractUser):
    profile_pic = models.ImageField(upload_to = 'profile_pics/', blank = True)
    username = models.CharField(max_length=191, unique=True)
    email = models.EmailField()
    first_name = models.CharField(max_length = 100, unique=False)
    last_name = models.CharField(max_length = 100, unique=False)
    articles = models.ManyToManyField(Article)

    def __str__(self):
        return self.username

    class Meta:
        unique_together = ('email',)
        verbose_name = 'User'

