$(document).ready(function() {
   $('#update_form').submit(function () {
       var that = $(this);
       var firstname = that[0][1]['value'];
       var lastname = that[0][2]['value'];
       var csrf = that.serializeArray()[0]['value'];
       $.ajax({
           type: 'POST',
           url: '/account/update_settings/',
           data: {csrfmiddlewaretoken: csrf,
                 firstname: firstname,
                 lastname: lastname,
                 },
           success: function(msg) {
               location.reload(true);
           },
           failure: function (msg) {
            alert('Please Try Again Later');
       }
       });
       return false;
   });
});