from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods, require_GET, require_POST
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse
from django.core import serializers
from django.conf import settings
from .models import Article
import json
import os
import base64
from datetime import datetime, date, timedelta
from math import log
#scoreCard = dict()

file_= os.path.join(settings.BASE_DIR, 'articleData.json')
def saveArticles():
    print str(datetime.now())
    with open(file_) as json_data:
        artdict = json.load(json_data)
        print "In here"
        for item in artdict:
            if item == None:
                continue
            title = item
            description = artdict[item]['description']
            if description == None:
                pass
            else:
                description = description[:300]
                description.lstrip('<p>').rstrip('</p>')
            destination_url = artdict[item]['link']
            try:
                date = artdict[item]['pubDate'].split(' ')
            except:
                continue
            M = ''
            if date[2] == 'Jan'or date[2] == 'JAN':
                M = '01'
            elif date[2] == 'Dec' or date[2] == 'DEC':
                M = '12'
            elif date[2] == 'Nov' or date[2] == 'NOV':
                M = '11'
            elif date[2] == 'Oct' or date[2] == 'OCT':
                M = '10'
            elif date[2] == 'Sep' or date[2] == 'SEP':
                M = '09'
            elif date[2] == 'Aug' or date[2] == 'AUG':
                M = '08'
            elif date[2] == 'Jul' or date[2] == 'JUL':
                M = '07'
            elif date[2] == 'Jun' or date[2] == 'JUN':
                M = '06'
            elif date[2] == 'May' or date[2] == 'MAY':
                M = '05'
            elif date[2] == 'Apr' or date[2] == 'APR':
                M = '04'
            elif date[2] == 'Mar' or date[2] == 'MAR':
                M = '03'
            elif date[2] == 'Feb' or date[2] == 'FEB':
                M = '02'

            publication_date = str(date[3]) +'-'+ M +'-'+ str(date[1])
            vocab_score = artdict[item]['vocab_score']
            source_name = artdict[item]['source']
            genre = artdict[item]['genre']
            contained_words = json.dumps(artdict[item]['word_list'])

            if Article.objects.filter(title=item).exists():
                print "Article exist"
                continue

            article = Article.objects.create(source_name = source_name, title = title, description = description, publication_date =  publication_date,  destination_url = destination_url, vocab_score = vocab_score, contained_words = contained_words)
            article.save()
            print "Article saved"
	print(len(artdict))
	print str(datetime.now())
        #for item in d:
        #    scoreCard[item] = int(d[item]['score'])

        #x = sorted(scoreCard.items(), key=lambda x:x[1])

def rankArticles():
    check_list = Article.objects.all()
    for i in check_list:
        try:
            score = hot(i.vocab_score, i.publication_date)
            i.rank = score
            i.save()
            print(i.title, " || ", i.rank, " || Success")
        except Exception as e:
            print(str(e))
            print (i.title, " || FAILURE")

def epoch_seconds(p_date):
    epoch = date(1970, 1, 1)
    td = p_date - epoch
    return (td.days * 86400)

def hot(score, p_date):
    order = log(max(abs(score), 1), 10)
    sign = 3 if score > 15 else 2 if score > 10 else 1 if score > 5 else 0 if score <= 0 else 0
    seconds = epoch_seconds(p_date) - 1134028003
    return round(sign * order + seconds / 45000, 7)

@require_POST
@csrf_exempt
@login_required
def addBookmark(request):
    user = request.user
    if request.method == 'POST' and request.is_ajax():
        a_title = request.POST['title']
        a_bookmark = Article.objects.get(title = a_title)
        user.articles.add(a_bookmark)
    return HttpResponse('Bookmark Saved')

@require_POST
@login_required
@csrf_exempt
def deleteBookmark(request):
    user = request.user
    if request.method == 'POST' and request.is_ajax():
        article_title = request.POST['title']
        user_article = user.articles.all()
        for item in user_article:
            if(item.title == article_title):
                user.articles.remove(item)
    return HttpResponse('Bookmark Deleted')

@require_POST
@login_required
def addOwnArticle(request):
	if request.method == 'POST' and request.is_ajax():
		own_url = request.POST['own_url']
		return HttpResponse('Article Bookmarked')
