from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as auth_login, logout as auth_logout, authenticate, update_session_auth_hash
from django.contrib.auth.tokens import default_token_generator
from django.views.decorators.http import require_http_methods, require_GET, require_POST
from django.views.generic import View
from django.views.generic.list import ListView
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import never_cache
from django.views.decorators.debug import sensitive_post_parameters
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger, InvalidPage
from django.core.urlresolvers import reverse
from django.utils.encoding import force_text
from django.utils.http import is_safe_url, urlsafe_base64_encode, urlsafe_base64_decode
from django.shortcuts import render, render_to_response, redirect, get_object_or_404
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect, Http404
from django.conf import settings
from django.core import serializers
from django.core.mail import EmailMessage
import base64
import json
import urllib
import urllib2

#from social.apps.django_app.default.models import UserSocialAuth

from .models import *
from account.forms import LoginForm, UserCreationForm, ContactForm, ForgotPasswordForm, ResetPasswordForm
from .tokens import password_reset_token
from article.models import *
from article.forms import filterForm
from material import *

# Create your views here.

@require_GET
def base(request):
    if request.user.is_authenticated():
        return redirect('home')
    else:
        lform = LoginForm()
        rform = UserCreationForm()
        cform = ContactForm()
        context = {'lform' : lform, 'rform' : rform, 'cform' : cform}
        return render(request, 'authentication/auth_base.html', context)

@require_http_methods(['GET', 'POST'])
@login_required
def home(request):
    if request.user.is_authenticated():
        f_form = filterForm 
        if request.method == 'GET':
            articlelist = Article.objects.order_by('-rank')
            context = {'f_form': f_form, 'articlelist': articlelist}
            return render(request, 'dashboard/dashboard.html', context)
        elif request.method == 'POST':
            form = filterForm(request.POST)
            if form.is_valid():
                genre = form.cleaned_data.get('genre')
                articlelist = Article.objects.filter(genre__in=genre).order_by('-rank')
                context = {'f_form': f_form, 'articlelist': articlelist}
                return render(request, 'dashboard/dashboard.html', context)
            else:
                articlelist = Article.objects.order_by('-rank')
                context = {'f_form': f_form, 'articlelist': articlelist}
                return render(request, 'dashboard/dashboard.html', context)
    else:
        return redirect('base')


def getMoreArticles(request):
    articlelist = Article.objects.order_by('-rank')
    paginator = Paginator(articlelist,10)
    if request.method == 'GET':
        if request.is_ajax():
            if request.GET.get('page_number'):
                #Paginate based on the page number in GET request
                page_number = request.GET.get('page_number')
                try:
                    page_objects = paginator.page(page_number).object_list
                except (InvalidPage, EmptyPage, PageNotAnInteger):
                    return HttpResponseBadRequest(mimetype="json")
                #Serialize the paginated objects
                resp = serializers.serialize('json', page_objects)
                return JsonResponse(resp, safe=False)
    articles = paginator.page(2).object_list
    print (articles)



@require_http_methods(['GET', 'POST'])
def login(request, **kwargs):
    if request.user.is_authenticated():
        return redirect(settings.LOGIN_REDIRECT_URL)   
    if request.method == 'GET':
        return redirect('base')
    else:
        form = LoginForm(request.POST)
        redirect_to = request.POST.get('next')
        if form.is_valid():
            try:
                user = form.get_user()
                auth_login(request, user)
                if not redirect_to or not is_safe_url(url=redirect_to, host = request.get_host()):
                    redirect_to = settings.LOGIN_REDIRECT_URL
                    return redirect(redirect_to)
                return redirect('home')
            except Exception as error:
                print(str(error))
        else:
            pass
        lform = LoginForm(request.POST)
        rform = UserCreationForm()
        cform = ContactForm()
        context = {'lform' : lform, 'rform' : rform, 'cform' : cform}
        return render(request, 'authentication/auth_base.html', context)


@login_required
def bookmarks(request):
    user = request.user
    if len(user.articles.all()) < 1:
        return render(request, 'bookmarks/nobookmarks.html')
    return render(request, 'bookmarks/bookmarks.html')

@login_required
@require_GET
def getBookmarks(request):
    user = request.user
    user_bookmarks = serializers.serialize('json', user.articles.all())
    return JsonResponse(user_bookmarks, safe=False)

@login_required
def acc_settings(request):
	user = request.user
	context = {'first_name': user.first_name, 'last_name': user.last_name, 'email': user.email}
	return render(request, 'settings/acc_settings.html', context)

@login_required
@require_POST
def update_settings(request):
	user = request.user
	user.first_name = request.POST['first_name2']
	user.last_name = request.POST['last_name2']
	user.save()
	context = {'first_name': user.first_name, 'last_name': user.last_name, 'email': user.email}
        return redirect('settings')

def validate_username(request):
    username = request.POST.get('username', None)
    data = {
            'is_taken': MyUser.objects.filter(username__iexact=username).exists()
            }
    if data['is_taken']:
        data['error_message'] = 'A user with this username already exists.'
    return JsonResponse(data)

@require_POST
def postsignup(request):
    form = UserCreationForm(data=request.POST)
    if form.is_valid():
        ''' Begin reCAPTCHA validation '''
        recaptcha_response = request.POST.get('g-recaptcha-response')
        url = 'https://www.google.com/recaptcha/api/siteverify'
        values = {
                'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                'response': recaptcha_response
                }

        data = urllib.urlencode(values)
        req = urllib2.Request(url, data)
        response = urllib2.urlopen(req)
        result = json.load(response)
        ''' End reCAPTCHA validation '''
        if result['success']:
            user = form.save()
            user.is_active = False
            user.save()
            url = request.build_absolute_uri(reverse('activate'))
            url =  url + "?user=" +base64.b64encode(user.username.encode('utf-8')).decode('utf-8')
            message = ''' Welcome to VocabX. Click  %s here to activate your account ''' %(url) 
            email = EmailMessage('Welcome', message, 'dailygoodwords@gmail.com', to=[user.email])
            email.send()
            return render(request, 'authentication/postsignup.html')
        else:
            lform = LoginForm()
            rform = UserCreationForm(request.POST)
            rform.errors['__all__'] = 'Invalid reCAPTCHA. Please try again.' #Non_Field_Error
            print rform.errors
            cform = ContactForm()
            context = {'lform' : lform, 'rform' : rform, 'cform' : cform}
            return render(request, 'authentication/auth_base.html', context)

    else:
        lform = LoginForm()
        rform = UserCreationForm(request.POST)
        cform = ContactForm()
        context = {'lform' : lform, 'rform' : rform, 'cform' : cform}
        return render(request, 'authentication/auth_base.html', context)

@require_GET
def activateaccount(request):
    if request.user.is_authenticated():
        return redirect('home')
    username = base64.b64decode(request.GET.get('user').encode('utf-8')).decode('utf-8')
    user = get_object_or_404(MyUser, username = username)
    user.is_active = True
    user.save()
    return redirect('base')

@require_GET
def logout(request):
    auth_logout(request)
    return redirect('base')

@require_http_methods(['GET', 'POST'])
def forgot_password(request):
    if request.method == "POST":
        form = ForgotPasswordForm(data=request.POST)
        if form.is_valid():
            opts = {
                    'token_generator': default_token_generator,
#                    'token_generator' : password_reset_token,
                    'from_email': settings.DEFAULT_FROM_EMAIL,
                    'email_template_name': 'registration/password_reset_email.txt',
                    'subject_template_name': 'registration/password_reset_subject.txt',
                    'request': request,
                    'html_email_template_name': None
                }
            form.save(**opts)
            return render(request, 'registration/password_reset_done.html')
    f_form = ForgotPasswordForm(request.POST)
    return render(request, 'registration/password_reset_form.html', {'f_form':f_form})

@require_http_methods(['GET','POST'])
@sensitive_post_parameters()
@never_cache
def reset_password(request, uidb64=None, token=None):
    if request.user.is_authenticated():
        return redirect(settings.LOGIN_REDIRECT_URL)

    assert uidb64 is not None and token is not None

    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = MyUser.objects.get(pk=uid)

    except (TypeError, ValueError, OverflowError, MyUser.DoesNotExist) as e:
        user = None

    if user is not None and default_token_generator.check_token(user, token):
#    if user is not None and password_reset_token.check_token(user, token):
        validlink = True
        if request.method == "POST":
            form = ResetPasswordForm(user, request.POST)
            if form.is_valid():
                form.save()
                return render(request, 'registration/password_reset_complete.html')
            print form.errors
        else:
            form = ResetPasswordForm(user)
    else:
        validlink = False
        form = None
    context = { 'validlink': validlink, 'form': form }
    return render(request, 'registration/password_reset_confirm.html', context)

@require_POST
def contact_us(request):
    cform = ContactForm
    form = cform(data=request.POST)
    if form.is_valid():
        ''' Begin reCAPTCHA validation '''
        recaptcha_response = request.POST.get('g-recaptcha-response')
        url = 'https://www.google.com/recaptcha/api/siteverify'
        values = {
                'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                'response': recaptcha_response
                }

        data = urllib.urlencode(values)
        req = urllib2.Request(url, data)
        response = urllib2.urlopen(req)
        result = json.load(response)
        ''' End reCAPTCHA validation '''
        print result['success']
        if result['success'] is True:
            name = request.POST.get('name', '')
            email = request.POST.get('email', '')
            content = request.POST.get('content','')
            message = "Name: "+name + '\n' + "Email: " + email + '\n' + "Message: " + content
            try:
                email = EmailMessage("New Contact Form Submission", message, to=['rohitchugh.95@gmail.com'], reply_to=[email])
                email.send()
            except Exception as e:
                print str(e)
            print "Message Sent"
            return HttpResponse('Message Sent')
        else:
            print "Invalid"
            return HttpResponse('Invalid reCAPTCHA. Please try again.')


        

class ActivateAccountView(View):
    def get(self, request, uidb64, token):
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = MyUser.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, MyUser.DoesNotExist):
            user = None

        if user is not None and account_activation_token.check_token(user, token):
            user.save()
            login(request, user)
            return redirect('home')
        else:
            #invalid link
            return render(request, 'registration/invalid.html')




@require_GET
@login_required
def search(request):
    context = { 'form' : SearchUserForm }
    return render(request, 'search/search_base.html', context)



@require_GET
@login_required
def search_users(request):
    name = request.GET.get('name')
    data = []
    if name:
        users = MyUser.objects.filter(first_name__icontains = name)
        print(users)
        data = [ { 'id' : user.id, 'name' : user.get_full_name()} for user in users]
        print(data)
        return JsonResponse(data = {'users' : data})

def getUserList(request):
    ulist = MyUser.objects.all()
    return render(request, 'account/userlist.html', {'ulist': ulist})

def getUserDetails(request, uname):
   #A BETTER SHORCUT WAY AND LOOSE COUPLING BETWEEN VIEWS AND MODELS(RECOMMENDED)
    user = get_object_or_404(MyUser, name = uname)
    return render(request, 'account/details.html', {'user': user})
    '''
    THIS IS ALSO CORRECT
    try:
        user = userDetails.objects.get(name = u_name)
    except userDetails.DoesNotExist:
        raise Http404("User does not exist.")
    return render(request, 'account/details.html', {'user': user})
    '''
'''
@login_required
def account_settings(request):
    user = request.user

    try:
        github_login = user.social_auth.get(provider='github')
    except UserSocialAuth.DoesNotExist:
        github_login = None
    
    try:
        twitter_login = user.social_auth.get(provider='twitter')
    except UserSocialAuth.DoesNotExist:
        twitter_login = None

    try:
        facebook_login = user.social_auth.get(provider='facebook')
    except UserSocialAuth.DoesNotExist:
        facebook_login = None

    can_disconnect = (user.social_auth.count() > 1 or user.has_usable.password())

    return render(request, 'settings/settings.html', {
        'github_login': github_login,
        'twitter_login': twitter_login,
        'facebook_login': facebook_login,
        'can_disconnect': can_disconnect
        })

@login_required
def password(request):
    if request.user.has_usable.password():
        PasswordForm = PasswordChangeForm
    else:
        PasswordForm = AdminPasswordChangeForm

    if request.method == 'POST':
        form = PasswordForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            messages.success(request, 'Your password was succesfully updated!')
            return redirect('password')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordForm(request.user)

    return render(request, 'settings/password.html', {'form': form})
'''
