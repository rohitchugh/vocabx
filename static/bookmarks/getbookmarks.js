$(document).ready(function() {
    var deleteFunction = function() {
        var del_title = $(this).parent().children(':first-child')[0].innerHTML;
        var item = $(this).parent().parent().parent();
        $.ajax({
            type: 'POST',
            url: '/article/deletebookmark/',
            data: {
                'title': del_title,
            },
            success: function(msg) {
                Materialize.toast('Bookmark Deleted', 1500, 'rounded');
                $(item).hide('slow', function() {
                    $(item).remove()
                });
            },
            failure: function() {
                console.log("FAILED TO DELETE.");
            }
        });
    };

    $.ajax({
        url: '/account/getbookmarks/',
        success: function(msg, status, xhr) {
            var ct = xhr.getResponseHeader("content-type") || "";
            if (ct == 'application/json') {

                var articles = JSON.parse(msg);
                if (articles.length < 1) {
    
                } else {
                    var list = document.getElementById('articles');
                    for (var i in articles) {
                        title = articles[i].fields.title;
                        link = articles[i].fields.destination_url
                        description = articles[i].fields.description;
                        score = articles[i].fields.vocab_score;
                        //wordsobj = JSON.parse(articles[i].fields.contained_words); GOOD WORDS IN ARTICLE

                        var card = document.createElement('div');
                        card.setAttribute('class', 'col s12 m12 l12 card-panel hoverable');

                        var card_content = document.createElement('div');
                        card_content.setAttribute('class', 'card-content ');

                        var card_title_div = document.createElement('div')
                        card_title_div.setAttribute('class', 'row')

                        var card_title = document.createElement('a')
                        card_title.setAttribute('class', 'card-title col s11 m11 l11 center-align article_title');
                        card_title.setAttribute('href', link);
                        card_title.setAttribute('target', '_blank');
                        card_title.textContent = title;

                        card_title_div.appendChild(card_title);

                        var card_del = document.createElement('i');
                        card_del.setAttribute('class', 'col s1 m1 l1 fa fa-trash tooltipped b_delete_icon');
                        card_del.setAttribute('data-position', 'bottom')
                        card_del.setAttribute('data-tooltip', 'Remove this article')
                        card_del.addEventListener('click', deleteFunction, false);

                        card_title_div.appendChild(card_del);

                        card_content.appendChild(card_title_div);

                        var card_description = document.createElement('p');
                        card_description.textContent = description;
                        card_content.appendChild(card_description);

                        card.appendChild(card_content);

                        var card_footer = document.createElement('div');
                        card_footer.setAttribute('class', 'card-action');

                        var card_score = document.createElement('span');
                        card_score.textContent = "Vocab Score: " + score;
                        card_score.setAttribute('class', 'right');

                        card_footer.appendChild(card_score);

                        card.appendChild(card_footer);

                        list.appendChild(card);

                        $('.tooltipped').tooltip({
                            delay: 50,
                            tooltip: 'Delete Bookmark',
                            position: 'top'
                        });
                    }
                }
            } else {
                window.alert('No Article Bookmarked')
            }
        },
        failure: function(foo) {
            console.log("FAILURE");
            Materialize.toast('No Internet Connection. Retry.', 5000, 'rounded');
        }
    });
});