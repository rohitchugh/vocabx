{% load i18n %}{% autoescape off %}
{% blocktrans %}You're receiving this email because you requested a password reset for your account at VocabX.{% endblocktrans %}

{% trans "Please go to the following page and choose a new password:" %}
{% block reset_link %}
	{{ protocol }}://{{ domain }}{% url 'password_reset_confirm' uidb64=uid token=token %}
{% endblock %}

If clicking the link above doesn't work, please copy and paste the URL in a new browser
window instead.

{% trans "Your username, in case you've forgotten:" %} {{ user.username }}

{% trans "Thanks for using our site!" %}

{% blocktrans %}VocabX Team{% endblocktrans %}

{% endautoescape %}
