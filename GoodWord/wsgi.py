"""
WSGI config for GoodWord project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os,sys
#sys.path.append('/opt/myenv/GoodWord')
#os.environ.setdefault("PYTHON_EGG_CACHE", "/opt/myenv/GoodWord/egg_cache") --In Deployment

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "GoodWord.settings")

application = get_wsgi_application()
