'''
    Python3
'''
from bs4 import BeautifulSoup
from multiprocessing import Pool
import xml.etree.cElementTree as ET
import requests

def getXML(url):
    '''
        Process XML data from website RSS feed
    '''
    artdict = dict()

    try:
        resp = requests.get(url)
        page = ET.XML(resp.content)
    except Exception as e:
        print(str(e))
        print("Error in url: ", url)
        return artdict
    
    tree = page.findall('channel')
    item_list = tree[0].findall('item')
    raw_source = url.split('.')
    source = raw_source[1]
    raw_genre = raw_source[2].split('/')

    if source in ('newyorker', 'nytimes'):
        raw_genre = raw_genre[len(raw_genre) - 1]
    elif(source == 'theguardian'):
        raw_genre = raw_genre[2]
    elif(source == 'thehindu'):
        raw_genre = raw_genre[1]
    else:
        raw_genre = 'others'
    
    raw_genre = raw_genre.strip()
    if raw_genre in ('Business', 'World', 'Education', 'Health', 'Science', 'Politics', 'Technology', 'Sports', 'Books', 'Culture'):
        genre = raw_genre
    elif raw_genre in ('business', 'news', 'world', 'education', 'health', 'science', 'technology', 'opinion', 'magazine', 'culture', 'cartoons', 'humor', 'others'):
        genre = raw_genre.title()
    elif raw_genre in ('tech', 'sci-tech'):
        genre = 'Technology' 
    elif raw_genre in ('sporting-scene', 'sport'):
        genre = 'Sports'
    elif raw_genre in ('book', 'arts'):
        genre = 'Books'
    elif raw_genre in ('humor, humour, Humor', 'Humour'):
        genre = 'Humour'
    else:
        genre = 'Others' #MostViewed, US

    for item in item_list:
        title = item[0].text
        if title is not None:
            title = title.strip()
            artdict[title] = dict()

            for foo in item[1:]:
                artdict[title][foo.tag] = foo.text

            artdict[title]['word_list'] = list()
            artdict[title]['vocab_score'] = 0
            artdict[title]['source'] = source
            artdict[title]['genre'] = genre

            if source == 'theguardian':
                artdict[title]['description'] = artdict[title]['description'].split('</p>')[0].lstrip('<p>')
    return artdict


def xml():
    '''
        Multiprocess Websites RSS feed
    '''
    how_many = 10
    p = Pool(processes=how_many)
    f_xml = open('rss.txt', 'r')
    parse_us = [ line.strip() for line in f_xml ]
    data = p.map(getXML, [link for link in parse_us])
    p.close()

    master_data = dict()
    for i in data:
        master_data.update(i)
    return master_data

if __name__ == '__main__':
    xml()
