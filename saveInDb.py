from pprint import pprint
import django
import json
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "GoodWord.settings")
django.setup()
from article.models import Article

path = '/home/rohit/Dropbox/GoodWord'
file_= os.path.join(path, 'articleData.json')

def save():
    with open(file_) as json_data:
        artdict = json.load(json_data)
        for item in artdict:
            if item == None:
                continue
            if Article.objects.filter(title=item).exists():
                print "Article exist"
                continue
            title = item
            description = artdict[item]['description']
            if description == None:
                pass
            else:
                description = description[:300]
                description.lstrip('<p>').rstrip('</p>')
            destination_url = artdict[item]['link']
            try:
                date = artdict[item]['pubDate'].split(' ')
            except:
                continue
            M = ''
            if date[2] == 'Jan'or date[2] == 'JAN':
                M = '01'
            elif date[2] == 'Dec' or date[2] == 'DEC':
                M = '12'
            elif date[2] == 'Nov' or date[2] == 'NOV':
                M = '11'
            elif date[2] == 'Oct' or date[2] == 'OCT':
                M = '10'
            elif date[2] == 'Sep' or date[2] == 'SEP':
                M = '09'
            elif date[2] == 'Aug' or date[2] == 'AUG':
                M = '08'
            elif date[2] == 'Jul' or date[2] == 'JUL':
                M = '07'
            elif date[2] == 'Jun' or date[2] == 'JUN':
                M = '06'
            elif date[2] == 'May' or date[2] == 'MAY':
                M = '05'
            elif date[2] == 'Apr' or date[2] == 'APR':
                M = '04'
            elif date[2] == 'Mar' or date[2] == 'MAR':
                M = '03'
            elif date[2] == 'Feb' or date[2] == 'FEB':
                M = '02'

            publication_date = str(date[3]) +'-'+ M +'-'+ str(date[1])
            vocab_score = artdict[item]['vocab_score']
            source_name = artdict[item]['source']
            genre = artdict[item]['genre']
            wordlist = ' '.join(artdict[item]['word_list']
            contained_words = wordlist
            
            article = Article.objects.create(source_name = source_name, title = title, description = description, publication_date =  publication_date,  destination_url = destination_url, vocab_score = vocab_score, contained_words = contained_words)
            article.save()
            print "Article saved"


save()
