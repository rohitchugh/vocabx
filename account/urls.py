from django.conf.urls import url
from django.contrib.auth import views as auth_views
from account import views
urlpatterns = [
        url(r'^login$', views.login, name="login"),
        url(r'^home/$', views.home, name="home"),
        url(r'^load_articles/$', views.getMoreArticles, name="load_articles"),
        url(r'^bookmarks/$', views.bookmarks, name="bookmarks"),
        url(r'^getbookmarks/$', views.getBookmarks, name="getbookmarks"),
        url(r'^settings/$', views.acc_settings, name='settings'),
        url(r'^update_settings/$', views.update_settings, name='update_settings'),
        url(r'^logout/$', views.logout, name="logout"),
        url(r'^ajax/validate_username/$', views.validate_username, name='validate_username'),
        url(r'^postsignup/$', views.postsignup, name="postsignup"),
        url(r'^activate/$', views.activateaccount, name="activate"),
        url(r'^forgot_password/$', views.forgot_password, name="forgot_password"),
#        url(r'^password_reset/$', auth_views.password_reset, name="password_reset"),
#        url(r'^password_reset/done/$', auth_views.password_reset_done , name="password_reset_done"),
#        url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', auth_views.password_reset_confirm, name="password_reset_confirm"),
#        url(r'^reset/done$', auth_views.password_reset_complete , name="password_reset_complete"),
        url(r'^password_reset_confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.reset_password, name="password_reset_confirm"),
        url(r'^search/$', views.search, name="search"),
        url(r'^search_users/$', views.search_users, name="search_users"),
#        url(r'^login/(?P<next>[a-z]+/)$', views.login, name="login"),
#        url(r'^settings/password$', views.password, name='password'),
#        url(r'^userlist$', views.getUserList, name="userList"),
#        url(r'^userdetails/(?P<uname>[A-Za-z\s]+)', views.getUserDetails, name="userDetails"),
]
