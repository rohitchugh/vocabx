from django.conf.urls import url
from article import views
urlpatterns =[
        url(r'^addbookmark/$', views.addBookmark, name="addbookmarks"),
        url(r'^deletebookmark/$', views.deleteBookmark, name="deletebookmark"),
		url(r'^addOwnArticle/$', views.addOwnArticle, name="addOwnArticle"),
]
