from __future__ import unicode_literals
from django.db import models
from django import forms
from django.forms import ModelForm, Form
from django.contrib.auth import authenticate
from django.contrib.auth.models import User, AbstractUser
from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm, SetPasswordForm
from django.utils.safestring import mark_safe
from django.utils.encoding import force_text
from django.utils.http import is_safe_url, urlsafe_base64_encode, urlsafe_base64_decode
from django.core.mail import send_mail
from django.template import loader
from functools import partial
from models import MyUser
from material import *
import base64
#Create your forms here.

class LoginForm(forms.ModelForm):
    error_css_class = 'error'
    password = forms.CharField(widget = forms.PasswordInput, required=True)
    keep_logged = forms.BooleanField(widget=forms.HiddenInput(), required=False, label="Keep me logged in")
    class Meta:
        model =  MyUser
        fields = ['username']

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.user_cache = None

    def clean(self):
        try:
            username = self.cleaned_data['username']
            password = self.cleaned_data['password']
        except:
            raise forms.ValidationError('Username and Password are required to login')
        if username and password:
            temp_user = MyUser.objects.filter(username = username)
            user_len = len(temp_user)
            
            if user_len < 1:
                raise forms.ValidationError('No account found with this username. Please register to login')
            elif temp_user[0].is_active == False:
                raise forms.ValidationError(mark_safe('<p class="center" style="font-size:1rem;">Click on activation link sent to registered mail id <br/> to activate your account <br/> OR <br/> Request a <a href="newlink" target="_blank">New Link</a></p>'))
            else:
                user = authenticate(username = username, password = password)
                if not user:
                    raise forms.ValidationError(mark_safe('<p class="center">Username and Password don\'t match!</p>'))
                self.user_cache = user
        else:
            pass
        return self.cleaned_data

    def get_user(self):
        return self.user_cache

class UserCreationForm(forms.ModelForm):
    error_css_class = 'error'
    password = forms.CharField(widget=forms.PasswordInput, required=True)
    password2 = forms.CharField(widget=forms.PasswordInput, required=True)
    class Meta:
        model = MyUser
        fields = ['username', 'email', 'first_name', 'last_name']
        labels = {
                'username' : 'Unique Username',
                'email' : 'Email',
                'first_name': 'First Name',
                'last_name': 'Last Name',
                }
        
    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['placeholder'] = self.fields[field].label           
        self.fields['password'].widget.attrs['placeholder'] = 'Password'
        self.fields['password2'].widget.attrs['placeholder'] = 'Password Confirm'
        self.fields['username'].widget.attrs['class'] = 'js-validate-username'

    def clean_password2(self):
        password1 = self.cleaned_data["password"]
        password2 = self.cleaned_data["password2"]
        
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(mark_safe('<p class="center red-text" style:"font-size:1rem;">Passwords do not match. Try again.</p>'))
        return password2

    def clean_email(self):
        try:
            email_data = self.cleaned_data.get("email")
        except:
            raise forms.ValdationError(mark_safe('<p class="cernter red-text" style:"font-size:1rem;">Please Enter your Email ID</p>'))
        user_len = len(MyUser.objects.filter(email = email_data))
        if email_data and user_len > 0:
            raise forms.ValidationError(mark_safe('<p class="center red-text" style:"font-size:1rem;">User with this email already exist!</p>'))
        return email_data

    def clean_username(self):
        try:
            username_data = self.cleaned_data.get("username")
        except:
            raise forms.ValidationError(mark_safe('<p class="center red-text" style="font-size:1rem;">Please Enter a Unique Username</p>'))
        if username_data and len(MyUser.objects.filter(username = username_data)) > 0:
            raise forms.ValidationError(mark_safe('<p class="center red-text" style="font-size:1rem;">Username taken. Please try with another username.</p>'))
        return username_data
    
    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit = False)
        user.set_password(self.cleaned_data['password'])
        if commit:
            user.save()
        return user

class ContactForm(forms.Form):
    name = forms.CharField(required = True)
    email = forms.EmailField(required = True)
    content = forms.CharField(required = True, widget=forms.Textarea)
    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['placeholder'] = 'Name'
        self.fields['email'].widget.attrs['placeholder'] = 'Email'
        self.fields['content'].widget.attrs['placeholder'] = 'Message'

class ForgotPasswordForm(forms.ModelForm):
    class Meta:
        model = MyUser
        fields = ['email']
        labels = {
                'email' : 'Email ID',
                }
    def __init__(self, *args, **kwargs):
        super(ForgotPasswordForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs['placeholder'] = self.fields['email'].label
        self.user_cache = None

    def clean(self):
        email_data = self.cleaned_data.get('email')
        if email_data and MyUser.objects.filter(email = email_data).count() == 0:
            self.user_cache=None
            raise forms.ValidationError(mark_safe("<p class='center red-text' style='font-size:1rem;'>We cannot find account with this email. Please verify your email address and try again.</p>"))

        elif email_data and MyUser.objects.filter(email = email_data).count() == 1:
            self.user_cache = MyUser.objects.filter(email = email_data)

        else:
            self.user_cache = None

        return self.cleaned_data


    def save(self, **kwargs):
        if self.user_cache is None:
            raise forms.ValidationError(mark_safe("<p class='center red-text' style='font-size:1rem;'>We cannot find account with this email. Please verify your email address and try again.</p>"))
        else:
            site_name = domain = 'vocabx.com'
            use_https = None
            user = self.user_cache[0]
            t_gen = kwargs['token_generator']
            context = {
                    'email' : user.email,
                    'domain' : site_name,
                    'site_name' : site_name,
                    'uid' : urlsafe_base64_encode(force_text(user.pk)),
                    'user' : user,
                    'token' : kwargs['token_generator'].make_token(user),
#                    'token' : t_gen._make_token_with_timestamp(user, t_gen._num_days(t_gen._today())),
                    'protocol' : use_https and 'https' or 'http',
                    }
            subject =  loader.render_to_string(kwargs['subject_template_name'], context) 
            subject = ''.join(subject.splitlines()) #subject must not contain newlines
            email = loader.render_to_string(kwargs['email_template_name'], context)
            try:
                send_mail(subject, email, kwargs['from_email'], [user.email])
            except Exception as e:
                print("Exception (send_mail) : ", str(e))
                raise forms.ValidationError(mark_safe("<p class='center red-text' style='font-size:1rem;'>Some Error Occured. Please try again after a few minutes. Inconvinience is deeply regretted.</p>"))
   
class ResetPasswordForm(SetPasswordForm):
    def __init__(self, user, *args, **kwargs):
        super(ResetPasswordForm, self).__init__(user, *args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['placeholder'] = self.fields[field].label           

'''
class ForgotPasswordForm(forms.Form):
    username = models.CharField(max_length=191)
    email = models.EmailField()

    def __init__(self, *args, **kwargs):
        super(ForgotPasswordForm, self).__init__(*args, **kwargs)

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if email and MyUser.objects.filter(email = email).count() == 0:
            raise forms.ValidationError(mark_safe("<p class='center red-text' style='font-size:1rem;'>We cannot find account with this email. Please verify your email address and try again.</p>"))
        return email

    def clean_username(self):
        username = self.cleaned_data.get('username')
        if username and MyUser.objects.filter(username = username).count() == 0:
            raise forms.ValidationError(mark_safe("<p class='center red-text' style='font-size:1rem;'>We cannot find account with this username. Please verify your username and try again.</p>"))
        email = MyUser.objects.filter(username = username).email
        return email

class UserCreationForm(forms.ModelForm):
#class UserCreationForm(forms.Form):
#    username = forms.CharField(max_lengt)
#    email = forms.EmailField(label="Email Address")
    password = forms.CharField(widget=forms.PasswordInput)
    password_confirm = forms.CharField(widget=forms.PasswordInput, label="Confirm password")
#    first_name = forms.CharField(required=False)
#    last_name = forms.CharField(required=False)
#    gender = forms.ChoiceField(choices=((None, ''), ('F', 'Female'), ('M', 'Male'), ('O', 'Other')))
#    receive_news = forms.BooleanField(required=False, label='I want to receive news and special offers')
#    agree_toc = forms.BooleanField(required=True, label='I agree with the Terms and Conditions')
    #passwd1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    #passwd2 = forms.CharField(label='Confirm Password', widget=forms.PasswordInput, help_text = "Should be same as Password")
    class Meta:
        model = MyUser;
        fields = ['username', 'email', 'first_name', 'last_name'];
    layout = Layout('username', 'email',
                    Row('password', 'password_confirm'),
                        Fieldset('Pesonal details',
                                 Row('first_name', 'last_name'),
                                      'gender', 'receive_news', 'agree_toc'))

    
    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        #for field in self.fields:
        #    self.fields[field].required = True
        #    self.fields[field].widget.attrs['class'] = 'input-field '
    def clean_password2(self):
        password = self.cleaned_data.get("password")
        password_confirm = self.cleaned_data.get("password_confirm")
        print("password", password, password_confirm)
        if password and password_confirm and password != password_confirm:
            raise forms.ValidationError("Passwords don't match!")
        return password_conford_confirmm

    def clean_email(self):
        email_data = self.cleaned_data.get("email")
        user_len = len(MyUser.objects.filter(email = email_data))
        if email_data and user_len > 0:
            print(email_data, " Sorry")
            raise forms.ValidationError("User with this email already exist!")
        return email_data

    def clean_username(self):
        username_data = self.cleaned_data.get("username")
        if username_data and len(MyUser.objects.filter(username = username_data)) > 0:
            raise forms.ValidationError("Username taken. Please enter another username.")
        return username_data

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit = False)
        user.set_password(self.cleaned_data['password'])
        if commit:
            user.save()
        return user


'''
class SearchUserForm(forms.Form):
    name = forms.CharField()
